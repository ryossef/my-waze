<?php
/**
* Plugin Name: MyWaze
* Plugin URI: http://savvy.co.il
* Description: Add a Waze navigation button to your mobile Wordpress site and get visitors navigate to your location in a click !
* Version: 1.4.1
* Author: Savvy Wordpress Development
* Author URI: http://savvy.co.il
* License: GPL2
*/


function my_waze_shortcode(){
$waze_options = get_option('my_waze_settings');
// if ( wp_is_mobile() ) {
    /* Display and echo mobile specific stuff here */
    return '<div id="mywaze" class="'.$waze_options['radio2'].'"><a id="'.$waze_options['radio1'].'" class="my_waze" href="waze://?ll='.$waze_options['my_waze_long'].','.$waze_options['my_waze_lat'].'&navigate=yes"></a></div>';

// }
}
add_shortcode('my_waze', 'my_waze_shortcode');


/**
 * Register with hook 'wp_enqueue_scripts', which can be used for front end CSS and JavaScript
 */
add_action( 'wp_enqueue_scripts', 'my_waze_add_my_stylesheet' );

/**
 * Enqueue plugin style-file
 */
function my_waze_add_my_stylesheet() {
    // Respects SSL, Style.css is relative to the current file
    wp_register_style( 'my_waze_style', plugins_url('style.css', __FILE__) );
    wp_enqueue_style( 'my_waze_style' );
}


/*
 * Add the admin page
 */
add_action('admin_menu', 'my_waze_admin_page');
function my_waze_admin_page(){
    add_menu_page('MyWaze Settings', 'MyWaze Settings', 'administrator', 'my_waze-settings', 'my_waze_admin_page_callback');
}

/*
 * Register the settings
 */
add_action('admin_init', 'my_waze_register_settings');
function my_waze_register_settings(){
    //this will save the option in the wp_options table as 'my_waze_settings'
    //the third parameter is a function that will validate your input values
    register_setting('my_waze_settings', 'my_waze_settings', 'my_waze_settings_validate');
}

function my_waze_settings_validate($args){
    //$args will contain the values posted in your settings form.
    if(!isset($args['my_waze_long']) || !isset($args['my_waze_lat']) || !isset($args['my_waze_address'])){
        //add a settings error because the form fields blank, so that the user can enter again
        $args['my_waze_long'] = '';
        $args['my_waze_lat'] = '';
        $args['my_waze_address'] = '';
        $args['my_waze_icon'] = '';
        $args['my_waze_align'] = '';
    add_settings_error('my_waze_settings', 'my_waze_no_data', 'Please make sure all the settings are configured.', $type = 'error');
    }

    //make sure you return the args
    return $args;
}

//Display the validation errors and update messages
/*
 * Admin notices
 */
add_action('admin_notices', 'my_waze_admin_notices');
function my_waze_admin_notices(){
   settings_errors();
}

//The markup for your plugin settings page
function my_waze_admin_page_callback(){ ?>

<style>

    #map {
        height: 360px;
        width: 100%;
    }
    input.waze_save {
        background: #e7442a;
        color: white;
        padding: 3px 20px;
        cursor: pointer;
    }
    input#my_waze_address {
        width: 400px;
    }
    input.waze_save:hover {
        background: #6c9f43;
    }
    .controls {
      margin-top: 10px;
      border: 1px solid transparent;
      border-radius: 2px 0 0 2px;
      box-sizing: border-box;
      -moz-box-sizing: border-box;
      height: 32px;
      outline: none;
      box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
      background-color: #fff;
      font-family: Roboto;
      font-size: 15px;
      font-weight: 300;
      margin-left: 12px;
      padding: 0 11px 0 13px;
      text-overflow: ellipsis;
      width: 300px;
    }

    #pac-input:focus {
      border-color: #4d90fe;
    }

    .pac-container {
      font-family: Roboto;
    }

    #type-selector {
      color: #fff;
      background-color: #4d90fe;
      padding: 5px 11px 0px 11px;
    }

    #type-selector label {
      font-family: Roboto;
      font-size: 13px;
      font-weight: 300;
    }
    .waze_icon {
        margin-right: 40px;
        display: inline-block;
    }
    .waze_icon input {
        display: inline-block;
        vertical-align: middle;
    }

    .waze_icon img {
        display: inline-block;
        vertical-align: middle;
    }

    .waze_align {
        margin-right: 35px;
        display: inline-block;
    }
</style>

<div class="wrap">
<h2>Waze Button Settings</h2><br />

<h3>Please enter the desired location:</h3>
<?php $options = get_option( 'my_waze_settings' );  ?>

<input id="pac-input" class="controls" type="text"
    placeholder="Enter a location">
<div id="map"></div>


<script>


    function initMap() {
      var opts = '<?php echo json_encode($waze_options); ?>';
      // console.log('OPTS:', opts);
      var lat = <?php echo (isset($options['my_waze_lat']) && $options['my_waze_lat'] != '') ? $options['my_waze_lat'] : '0'; ?>;
      var lng = <?php echo (isset($options['my_waze_long']) && $options['my_waze_long'] != '') ? $options['my_waze_long'] : '0'; ?>;
      var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: 13
      });
      var input = /** @type {!HTMLInputElement} */(
          document.getElementById('pac-input'));

      var types = document.getElementById('type-selector');
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.bindTo('bounds', map);

      var infowindow = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
      });


      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
          window.alert("Autocomplete's returned place contains no geometry");
          return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          map.fitBounds(place.geometry.viewport);
        } else {
          map.setCenter(place.geometry.location);
          map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
          address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);

        var place = autocomplete.getPlace();
        jQuery('#my_waze_long').val(place.geometry.location.lng());
        jQuery('#my_waze_lat').val(place.geometry.location.lat());
        jQuery('#my_waze_address').val(place.formatted_address);
        console.log(place.formatted_address);

      });

      // Sets a listener on a radio button to change the filter type on Places
      // Autocomplete.


    }

</script>



    <form action="options.php" method="post"><?php
        settings_fields( 'my_waze_settings' );


        do_settings_sections( __FILE__ );
        $dir = plugins_url();
        //get the older values, wont work the first time
        ?>
        <br/>
        <h3>Current Location:</h3>
        <input name="my_waze_settings[my_waze_long]" readonly type="text" id="my_waze_long" value="<?php echo (isset($options['my_waze_long']) && $options['my_waze_long'] != '') ? $options['my_waze_long'] : ''; ?>"/>
        <input name="my_waze_settings[my_waze_lat]" readonly type="text" id="my_waze_lat" value="<?php echo (isset($options['my_waze_lat']) && $options['my_waze_lat'] != '') ? $options['my_waze_lat'] : ''; ?>"/>
        <input name="my_waze_settings[my_waze_address]" readonly type="text" id="my_waze_address" value="<?php echo (isset($options['my_waze_address']) && $options['my_waze_address'] != '') ? $options['my_waze_address'] : ''; ?>"/>

        <fieldset>
            <legend><br/><h3>Please select one of the following icons:</h3><br/></legend>
            <div class="waze_icon"><input type="radio" name="my_waze_settings[radio1]" value="item1" <?php checked('item1', $options['radio1']); ?> />
            <img src="<?php echo $dir ?>/my-waze/img/icon1.png"></div>

            <div class="waze_icon"><input type="radio" name="my_waze_settings[radio1]" value="item2" <?php checked('item2', $options['radio1']); ?> />
            <img src="<?php echo $dir ?>/my-waze/img/icon2.png"></div>

            <div class="waze_icon"> <input type="radio" name="my_waze_settings[radio1]" value="item3" <?php checked('item3', $options['radio1']); ?> />
            <img src="<?php echo $dir ?>/my-waze/img/icon3.png"></div>

            <div class="waze_icon"> <input type="radio" name="my_waze_settings[radio1]" value="item4" <?php checked('item4', $options['radio1']); ?> />
            <img src="<?php echo $dir ?>/my-waze/img/icon4.png"></div>
        </fieldset>

        <br/>

        <fieldset>
            <legend><br/><h3>Please select the icon alignment:</h3><br/></legend>
            <div class="waze_align">
            <input type="radio" name="my_waze_settings[radio2]" id="left" value="left" <?php checked('left', $options['radio2']); ?> />
            <label for="left">Left</label></div>
            <div class="waze_align"><input type="radio" id="center" name="my_waze_settings[radio2]" value="center" <?php checked('center', $options['radio2']); ?> />
            <label for="left">Center</label></div>
            <div class="waze_align"><input type="radio" id="right" name="my_waze_settings[radio2]" value="right" <?php checked('right', $options['radio2']); ?> />
            <label for="left">Right</label></div>

        </fieldset>


        <br/><br/>
        <input class="waze_save" type="submit" value="Save" />
    </form>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD06iUyqFzxx63m-hFewtXmAILuBGQadO8&signed_in=true&libraries=places&callback=initMap"
        async defer></script>
</div>
<?php }



