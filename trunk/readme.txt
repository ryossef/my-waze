=== Plugin Name ===
Contributors: roeeyossef
Tags: Waze, Navigation, Mobile
Requires at least: 4.0
Tested up to: 4.3.1
Stable tag: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add a Waze navigation button to your mobile Wordpress site and get visitors navigate to your location in a click !

== Description ==

MyWaze is the first plugin created that let you integrate a Waze navigation button in your Wordpress site in a simple way.



== Installation ==

1. Download the Plugin.
2. Upload `my-waze` folder to the `/wp-content/plugins/` directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Go to MyWaze Settings section on the wordpress admin panel and enter your location via Google Maps.
5. Choose an icon of your choice.
6. Select the alignment of the button.
7. Place **[my_waze]** shortcode wherever you want the button to appear your page.

== Frequently Asked Questions ==

= Why can't i see the button ? =

The plugin use the **wp_is_mobile()** function so the button should appear **only on mobile devices**.


== Screenshots ==


== Changelog ==

= Version 1.1.0 =

BUGFIX - fixed issue - fixed a CSS error which prevented the icon from showing

= Version 1.2.0 =

Improvement - Icon replaced to a modern icon

= Version 1.3.0 =

BUGFIX - fixed issue - ixed an issue with the new icon

= Version 1.3.1 =

BUGFIX - fixed issue - icon always appears on top

= Version 1.3.2 =

BUGFIX - fixed issue - icon appears on wrong

= Version 1.4 =

**A complete redesign of the plugin !** 

1. Choose your location using Google Maps API.
2. Ability to choose the icon out of 4 different icons.
3. Ability to choose the icon alignment.

= Version 1.4.1 =

fix an issue with the new icons